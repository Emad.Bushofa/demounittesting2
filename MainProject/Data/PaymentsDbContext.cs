﻿using Microsoft.EntityFrameworkCore;

namespace MainProject.Data
{
    public class PaymentsDbContext : DbContext
    {
        public PaymentsDbContext(DbContextOptions<PaymentsDbContext> options) : base(options)
        {

        }

        public DbSet<Deposit> Deposits { get; set; }
    }

    public class Deposit
    {
        public Guid Id { get; set; }
        public string? AccountNumber { get; set; }
        public decimal Value { get; set; }
        public DateTime DepositedAt { get; set; }
    }
}
