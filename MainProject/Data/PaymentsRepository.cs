﻿using Microsoft.EntityFrameworkCore;

namespace MainProject.Data
{
    public class PaymentsRepository
    {
        private readonly PaymentsDbContext _context;

        public PaymentsRepository(PaymentsDbContext context)
        {
            _context = context;
        }

        public Task<int> GetTotalDepositsTodayAsync(string accountNumber) =>
            _context.Deposits.CountAsync(x => x.AccountNumber == accountNumber && x.DepositedAt.Date == DateTime.UtcNow.Date);

        public Task AddDepositAsync(Deposit deposit) => _context.Deposits.AddAsync(deposit).AsTask();
    }

    public class UnitOfWork
    {
        private readonly PaymentsDbContext _context;

        public UnitOfWork(PaymentsDbContext context)
        {
            _context = context;
            Payments = new PaymentsRepository(context);
        }

        public PaymentsRepository Payments { get; }

        public Task CompleteAsync() => _context.SaveChangesAsync();
    }
}
