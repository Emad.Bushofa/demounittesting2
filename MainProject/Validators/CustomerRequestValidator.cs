﻿using FluentValidation;
using MainProject.CustomersProto;

namespace MainProject.Validators
{
    public class ValidateRequestValidator : AbstractValidator<ValidateRequest>
    {
        public ValidateRequestValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Address).NotEmpty();
            RuleFor(x => x.Age).InclusiveBetween(18, 65);
        }
    }
}
