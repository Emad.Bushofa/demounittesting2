using Calzolari.Grpc.AspNetCore.Validation;
using MainProject.Data;
using MainProject.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddGrpc(o => o.EnableMessageValidation());

builder.Services.AddValidators();
builder.Services.AddGrpcValidation();
builder.Services.AddScoped<UnitOfWork>();

builder.Services.AddDbContext<PaymentsDbContext>(options => options.UseSqlServer(""));

builder.Host.UseSerilog();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<CustomersGrpcService>();
app.MapGrpcService<PaymentsService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();

public partial class Program { }
