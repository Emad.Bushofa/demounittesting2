﻿using Grpc.Core;
using MainProject.Data;
using MainProject.PaymentsProto;

namespace MainProject.Services
{
    public class PaymentsService : Payments.PaymentsBase
    {
        private readonly UnitOfWork _unitOfWork;

        public PaymentsService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<DepositResponse> Deposit(DepositRequest request, ServerCallContext context)
        {
            if (await _unitOfWork.Payments.GetTotalDepositsTodayAsync(request.AccountNumber) >= 2)
            {
                throw new RpcException(new Status(StatusCode.FailedPrecondition, ""));
            }

            var deposit = new Deposit()
            {
                AccountNumber = request.AccountNumber,
                Value = (decimal)request.Value,
                DepositedAt = DateTime.UtcNow,
            };
            await _unitOfWork.Payments.AddDepositAsync(deposit);
            await _unitOfWork.CompleteAsync();

            return new DepositResponse()
            {
                DepositId = deposit.Id.ToString(),
            };
        }
    }
}
