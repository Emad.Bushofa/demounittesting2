using Grpc.Core;
using MainProject.CustomersProto;

namespace MainProject.Services
{
    public class CustomersGrpcService : Customers.CustomersBase
    {
        public override Task<ValidateResponse> Validate(ValidateRequest request, ServerCallContext context)
        {
            return Task.FromResult(new ValidateResponse());
        }
    }

}
