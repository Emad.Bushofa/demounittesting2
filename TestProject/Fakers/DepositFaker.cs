﻿using Bogus;
using MainProject.Data;

namespace TestProject.Fakers
{
    public class DepositFaker : Faker<Deposit>
    {
        public DepositFaker()
        {
            RuleFor(x => x.Value, f => f.Random.Int(1000, 10000));
            RuleFor(x => x.AccountNumber, f => $"{f.Random.Int(10000000, 99999999)}-{f.Random.Int(1, 9)}");
            RuleFor(x => x.DepositedAt, f => f.Date.Past());
        }
    }
}
