﻿using Grpc.Core;
using MainProject.Data;
using MainProject.Services;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestProject.Fakers;
using TestProject.PaymentsProto;
using Xunit.Abstractions;

namespace TestProject
{
    public class PaymnetsGrpcServiceTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public PaymnetsGrpcServiceTests(WebApplicationFactory<Program> factory, ITestOutputHelper testOutput)
        {
            _factory = factory.WithDefaultConfigurations(testOutput, services =>
            {
                var descriptor = services.Single(d => d.ServiceType == typeof(DbContextOptions<PaymentsDbContext>));
                services.Remove(descriptor);
                var dbName = Guid.NewGuid().ToString();
                services.AddDbContext<PaymentsDbContext>(options => options.UseInMemoryDatabase(dbName));
            });
        }

        [Theory]
        [InlineData("11223344-1", 1000)]
        [InlineData("27095635-1", 2000)]
        [InlineData("00000000-0", 9000)]
        [InlineData("00000000-9", 9000)]
        [InlineData("99999999-1", 10000)]
        [InlineData("99999999-0", 10000)]
        [InlineData("99999999-9", 10000)]
        public async Task Deposit_SendValidData_DepositSaved(string accountNumber, double value)
        {
            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var response = await client.DepositAsync(new DepositRequest()
            {
                AccountNumber = accountNumber,
                Value = value,
            });

            using var scope = _factory.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

            var deposits = await context.Deposits.ToListAsync();

            Assert.NotEmpty(response.DepositId);
            var deposit = Assert.Single(deposits);
            Assert.Equal(response.DepositId, deposit.Id.ToString());
            Assert.Equal(accountNumber, deposit.AccountNumber);
            Assert.Equal((decimal)value, deposit.Value);
            Assert.Equal(DateTime.UtcNow, deposit.DepositedAt, TimeSpan.FromSeconds(1));
        }

        [Fact]
        public async Task Deposit_WithSingleDepositAlreadyExistedInThisDayWithTheSameAccount_ThirdDepositShouldBeFailed()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Deposits.Add(new Deposit()
                {
                    AccountNumber = "11223344-1",
                    Value = 1000,
                    DepositedAt = DateTime.UtcNow
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var response = await client.DepositAsync(new DepositRequest()
            {
                AccountNumber = "11223344-1",
                Value = 1000,
            });

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

                var deposits = await context.Deposits.ToListAsync();
                Assert.Equal(2, deposits.Count);
                Assert.Equal(response.DepositId, deposits.Last().Id.ToString());
            }
        }

        [Fact]
        public async Task Deposit_WithTwoDepositsAlreadyExistedInThisDayWithTheSameAccount_ThirdDepositShouldBeFailed()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Deposits.AddRange(new DepositFaker()
                    .RuleFor(x => x.AccountNumber, "11223344-1")
                    .RuleFor(x => x.DepositedAt, DateTime.UtcNow)
                    .Generate(2));
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var ex = await Assert.ThrowsAsync<RpcException>(() => client.DepositAsync(new DepositRequest()
            {
                AccountNumber = "11223344-1",
                Value = 1000,
            }).ResponseAsync);

            Assert.Equal(StatusCode.FailedPrecondition, ex.StatusCode);

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

                var deposits = await context.Deposits.ToListAsync();
                Assert.Equal(2, deposits.Count);
            }
        }

        [Fact]
        public async Task Deposit_WithTwoDepositsAlreadyExistedInThisDayButOnlyOneOfThemWithTheSameAccount_DepositSaved()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Deposits.Add(new Deposit()
                {
                    AccountNumber = "11223344-0",
                    Value = 1000,
                    DepositedAt = DateTime.UtcNow
                });
                context.Deposits.Add(new Deposit()
                {
                    AccountNumber = "11223344-1",
                    Value = 1000,
                    DepositedAt = DateTime.UtcNow
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var response = await client.DepositAsync(new DepositRequest()
            {
                AccountNumber = "11223344-1",
                Value = 1000,
            });

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

                var deposits = await context.Deposits.ToListAsync();
                Assert.Equal(3, deposits.Count);
                Assert.Equal(response.DepositId, deposits.Last().Id.ToString());
            }
        }

        [Fact]
        public async Task Deposit_WithTwoDepositsAlreadyExistedForTheSameAccountButOneOfThemIsNotWithinTheSameDay_DepositSaved()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Deposits.Add(new Deposit()
                {
                    AccountNumber = "11223344-1",
                    Value = 1000,
                    DepositedAt = DateTime.UtcNow.AddDays(-1)
                });
                context.Deposits.Add(new Deposit()
                {
                    AccountNumber = "11223344-1",
                    Value = 1000,
                    DepositedAt = DateTime.UtcNow
                });
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var response = await client.DepositAsync(new DepositRequest()
            {
                AccountNumber = "11223344-1",
                Value = 1000,
            });

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

                var deposits = await context.Deposits.ToListAsync();
                Assert.Equal(3, deposits.Count);
                Assert.Equal(response.DepositId, deposits.Last().Id.ToString());
            }
        }

        [Fact]
        public async Task Deposit_WithThreeDepositsAlreadyExistedInThisDayWithTheSameAccount_ThirdDepositShouldBeFailed()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();
                context.Deposits.AddRange(new DepositFaker()
                    .RuleFor(x => x.AccountNumber, "11223344-1")
                    .RuleFor(x => x.DepositedAt, DateTime.UtcNow)
                    .Generate(3));
                await context.SaveChangesAsync();
            }

            var client = new Payments.PaymentsClient(_factory.CreateGrpcChannel());

            var ex = await Assert.ThrowsAsync<RpcException>(() => client.DepositAsync(new DepositRequest()
            {
                AccountNumber = "11223344-1",
                Value = 1000,
            }).ResponseAsync);

            Assert.Equal(StatusCode.FailedPrecondition, ex.StatusCode);

            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<PaymentsDbContext>();

                var deposits = await context.Deposits.ToListAsync();
                Assert.Equal(3, deposits.Count);
            }
        }
    }
}
