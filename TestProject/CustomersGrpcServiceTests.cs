﻿using Grpc.Core;
using MainProject.Services;
using Microsoft.AspNetCore.Mvc.Testing;
using TestProject.CustomersProto;

namespace TestProject
{
    public class CustomersGrpcServiceTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public CustomersGrpcServiceTests(WebApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("John", "123", 32)]
        [InlineData("a", "w", 18)]
        [InlineData("a", "w", 65)]
        public async Task Validate_AllInputsHaveValidData_IsValidEqualsTrue(
            string name,
            string address,
            int age
        )
        {
            var client = new Customers.CustomersClient(_factory.CreateGrpcChannel());

            await client.ValidateAsync(new ValidateRequest()
            {
                Name = name,
                Address = address,
                Age = age
            });
        }

        [Theory]
        [InlineData("", "123", 32)]
        [InlineData(" ", "123", 32)]
        [InlineData("abc", "", 32)]
        [InlineData("abc", " ", 32)]
        [InlineData("abc", "123", 17)]
        [InlineData("abc", "123", 0)]
        [InlineData("abc", "123", 66)]
        [InlineData("abc", "123", 100)]
        public async Task Validate_SomeInputsHaveInvalidData_IsValidEqualsFalse(
            string? name,
            string? address,
            int age
        )
        {
            var client = new Customers.CustomersClient(_factory.CreateGrpcChannel());

            var ex = await Assert.ThrowsAsync<RpcException>(() => client.ValidateAsync(new ValidateRequest()
            {
                Name = name,
                Address = address,
                Age = age
            }).ResponseAsync);

            Assert.Equal(StatusCode.InvalidArgument, ex.StatusCode);
        }
    }
}