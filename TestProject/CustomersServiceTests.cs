//using MainProject;

//namespace TestProject
//{
//    public class CustomersServiceTests
//    {

//        [Theory]
//        [InlineData("John", "123", 32)]
//        [InlineData("a", "w", 18)]
//        [InlineData("a", "w", 65)]
//        public void Validate_AllInputsHaveValidData_IsValidEqualsTrue(
//            string name,
//            string address,
//            int age
//        )
//        {
//            var customerService = new CustomersService();
//            var customer = new Customer { Name = name, Age = age, Address = address };

//            var result = customerService.Validate(customer);

//            Assert.True(result.IsValid);
//        }

//        [Theory]
//        [InlineData(null, "123", 32)]
//        [InlineData("abc", null, 32)]
//        [InlineData("abc", "123", 17)]
//        [InlineData("abc", "123", 0)]
//        [InlineData("abc", "123", 66)]
//        [InlineData("abc", "123", 100)]
//        public void Validate_SomeInputsHaveInvalidData_IsValidEqualsFalse(
//            string? name,
//            string? address,
//            int age
//        )
//        {
//            var customerService = new CustomersService();
//            var customer = new Customer { Name = name, Age = age, Address = address };

//            var result = customerService.Validate(customer);

//            Assert.False(result.IsValid);
//        }
//    }
//}